import React from "react";
import { Jumbotron as Jumbo, Container} from "react-bootstrap";
import work from "../imgslider.jpg"
import styled from "styled-components";

const Styles = styled.div `
    .jumbo {
        background: url(${work}) no-repeat fixed bottom;
        background-size: cover;
        color: #efefef;
        height: 400px;
        position: relative;
        z-index: -2
    }
    .overlay {
        background-color: #000;
        opacity: 0.7;
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        z-index: -1;
    }
`

const Jumbotron = () => {
    return(
        <Styles>
            <Jumbo fluid className="jumbo">
                <div className="overlay"></div>
                <Container>
                    <h1>Web Developer Blog</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        A at beatae cumque distinctio inventore libero, magni maxime nesciunt
                        odio perspiciatis placeat possimus
                        quae quibusdam repellat rerum sapiente sint temporibus veniam?
                        cumque distinctio inventore libero, magni maxime nesciunt
                        odio perspiciatis placeat possimus
                        quae quibusdam repellat rerum sapiente sint temporibus veniam?
                        cumque distinctio inventore libero, magni maxime nesciunt
                        odio perspiciatis placeat possimus
                        quae quibusdam repellat rerum sapiente sint temporibus veniam?
                        cumque distinctio inventore libero, magni maxime nesciunt
                        odio perspiciatis placeat possimus
                        quae quibusdam repellat rerum sapiente sint temporibus veniam?
                        cumque distinctio inventore libero, magni maxime nesciunt
                        odio perspiciatis placeat possimus
                        quae quibusdam repellat rerum sapiente sint temporibus veniam?</p>
                </Container>
            </Jumbo>
        </Styles>
    )
}

export default Jumbotron