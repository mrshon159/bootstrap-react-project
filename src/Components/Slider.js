import React from "react";
import {Carousel} from "react-bootstrap";
import work from "../imgslider.jpg"

const Slider = () => {
    return(
        <Carousel>
            <Carousel.Item style={{'height': '550px'}}>
                <img
                    className="d-blog w-100"
                    src={work}
                    alt="first slide"
                />
                <Carousel.Caption>
                    <h3>Slider first.</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aperiam, delectus explicabo fugiat fugit, illum maxime necessitatibus neque nesciunt nisi numquam omnis pariatur quasi quos sapiente sint veritatis vitae voluptatem.</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item style={{'height': '550px'}}>
                <img
                    className="d-blog w-100"
                    src={work}
                    alt="first slide"
                />
                <Carousel.Caption>
                    <h3>Slider second.</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aperiam, delectus explicabo fugiat fugit, illum maxime necessitatibus neque nesciunt nisi numquam omnis pariatur quasi quos sapiente sint veritatis vitae voluptatem.</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item style={{'height': '550px'}}>
                <img
                    className="d-blog w-100"
                    src={work}
                    alt="first slide"
                />
                <Carousel.Caption>
                    <h3>Slider third.</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aperiam, delectus explicabo fugiat fugit, illum maxime necessitatibus neque nesciunt nisi numquam omnis pariatur quasi quos sapiente sint veritatis vitae voluptatem.</p>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>
    )
}

export default Slider