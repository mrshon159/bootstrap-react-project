import React, {useState} from "react";
import {Button, Container, Form, Modal, Nav, Navbar} from "react-bootstrap";
import {Link} from "react-router-dom";
import styled from 'styled-components';

const Styles = styled.div `
    a, .navbar-brand, .navbar-nav, .nav-link {
        color: #adb1b8;
        &:hover {
            color: white
        }
    }
`


const Navibar = () => {

    const [showLogin, setShowLogin] = useState(false)
    const [showSignIn, setShowSignIn] = useState(false)

    const handleClose = () => {
        setShowLogin(false)
        setShowSignIn(false)
    }
    const handleShowLogin = () => {
        setShowLogin(true)
    }
    const handleShowSignIn = () => {
        setShowSignIn(true)
    }
    return(
        <>
            <Styles>
                    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                        <Container>
                            <Navbar.Brand>React-bootstrap</Navbar.Brand>
                            <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                            <Navbar.Collapse id="responsive-navbar-nav">
                                <Nav className="mr-auto">
                                    <Nav.Link><Link to="/">Home</Link></Nav.Link>
                                    <Nav.Link><Link to="/users">Users</Link></Nav.Link>
                                    <Nav.Link><Link to="/about">About</Link></Nav.Link>
                                </Nav>
                                <Nav>
                                    <Button variant="primary" className="mr-2" onClick={handleShowLogin}>Log In</Button>
                                    <Button variant="primary" onClick={handleShowSignIn}>Sign In</Button>
                                </Nav>
                            </Navbar.Collapse>
                        </Container>
                    </Navbar>
            </Styles>
            <Modal show={showLogin} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Log in</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="fromBasicEmail">
                            <Form.Label>Email Address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email"/>
                            <Form.Text className="text-muted">We'll newer share your email with anyone else</Form.Text>
                        </Form.Group>
                        <Form.Group controlId="fromBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Enter password"/>
                        </Form.Group>
                        <Form.Group controlId="fromBasicCheckbox" >
                            <Form.Check type="checkbox" className="mr-5" label="Remember me" />
                        </Form.Group>
                        <div className="text-right">
                            <Button variant="primary">Log In</Button>
                        </div>
                    </Form>
                </Modal.Body>
            </Modal>
            <Modal show={showSignIn} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Sign in</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="fromBasicEmail">
                            <Form.Label>Email Address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email"/>
                            <Form.Text className="text-muted">We'll newer share your email with anyone else</Form.Text>
                        </Form.Group>
                        <Form.Group controlId="fromBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Enter password"/>
                        </Form.Group>
                        <Form.Group controlId="fromBasicPassword">
                            <Form.Label>Repeat password</Form.Label>
                            <Form.Control type="password" placeholder="Repeat password"/>
                        </Form.Group>
                        <div className="text-right">
                            <Button variant="primary">Sign In</Button>
                        </div>
                    </Form>
                </Modal.Body>
            </Modal>
        </>
    )
}

export default Navibar