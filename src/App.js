import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";

import Navibar from "./Components/Navibar";
import Footer from "./Components/Footer";

import {Home} from "./Home"
import {Users} from "./Users"
import {About} from "./About"

function App() {
  return (
    <>
        <Router>
            <Navibar />
            <Switch>
                <Route exact path="/" component={Home}></Route>
                <Route path="/users" component={Users}></Route>
                <Route path="/about" component={About}></Route>
            </Switch>
        </Router>
        <Footer />
    </>
  );
}

export default App;
