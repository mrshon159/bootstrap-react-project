import React from "react";
import Slider from "./Components/Slider";
import {Button, Card, Col, Container, Row} from "react-bootstrap";

import cardwork from "./cardwork.jpg"
import Jumbotron from "./Components/Jumbotron";

export const Home = () => {
    return (
        <>
            <Slider />
            <Container style={{paddingTop: '2rem', paddingBottom: '2rem'}}>
                <Row>
                    <Col>
                       <Card style={{width: '18rem'}}>
                            <Card.Img variant="top" src={cardwork}/>
                            <Card.Body>
                                <Card.Title>WebDev Blog</Card.Title>
                                <Card.Text>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aliquid architecto assumenda deserunt dignissimos, distinctio eligendi enim eos esse fugiat labore minus odio optio, repellendus sint sunt, vel veritatis voluptates?</Card.Text>
                                <Button variant="primary">Learn more</Button>
                            </Card.Body>
                       </Card>
                    </Col>
                    <Col>
                        <Card style={{width: '18rem'}}>
                            <Card.Img variant="top" src={cardwork}/>
                            <Card.Body>
                                <Card.Title>WebDev Blog</Card.Title>
                                <Card.Text>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aliquid architecto assumenda deserunt dignissimos, distinctio eligendi enim eos esse fugiat labore minus odio optio, repellendus sint sunt, vel veritatis voluptates?</Card.Text>
                                <Button variant="primary">Learn more</Button>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col>
                        <Card style={{width: '18rem'}}>
                            <Card.Img variant="top" src={cardwork}/>
                            <Card.Body>
                                <Card.Title>WebDev Blog</Card.Title>
                                <Card.Text>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aliquid architecto assumenda deserunt dignissimos, distinctio eligendi enim eos esse fugiat labore minus odio optio, repellendus sint sunt, vel veritatis voluptates?</Card.Text>
                                <Button variant="primary">Learn more</Button>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
            <Jumbotron />
            <Container style={{marginBottom: '30px'}}>
                <Row>
                    <Col md={8}>
                        <img src={cardwork} height={400}/>
                    </Col>
                    <Col md={4}>
                        <h2>Web Developer Blog</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            A at beatae cumque distinctio inventore libero, magni maxime nesciunt
                            odio perspiciatis placeat possimus
                            quae quibusdam repellat rerum sapiente sint temporibus veniam?
                            cumque distinctio inventore libero, magni maxime nesciunt
                            odio perspiciatis placeat possimus
                            quae quibusdam repellat rerum sapiente sint temporibus veniam?
                            </p>
                    </Col>
                </Row>
            </Container>
         </>
    )
}
